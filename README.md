# Rencontres R 2021  

[RR2021](https://paris2021.rencontresr.fr)

* Date : le 12 et 13 Juillet 2021
* Lieu : AgroParisTech, Paris
* Programme : [https://rr2021.sciencesconf.org/program](https://rr2021.sciencesconf.org/program)


## Présentation

* Slides [Environnement R et GitLab CI/CD, J.F. Rey, L. Houde](GitLabCICD_R_RENCONTRESR2021.pdf)
* Dépôt HAL : [https://hal.archives-ouvertes.fr/hal-03286897](https://hal.archives-ouvertes.fr/hal-03286897)
* [Vidéo de la présentation](https://youtu.be/D5mE8AaRnw0)


